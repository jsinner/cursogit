<?php

namespace Modelo;

use Modelo\Modelo;

class Usuario extends Modelo {

    /**
     * @var int id
     */
    protected $id;

    /**
     * @var string nombre
     */
    protected $nombre;

    /**
     * @var string apellido
     */
    protected $apellido;

    /**
     * @var string email
     */
    protected $email;

    /**
     * Contructor de la Clase Modelo\Usuario
     * @param int $id
     * @param string $nombre
     * @param string $apellido
     * @param string $email
     */
    public function __construct($nombre = null, $apellido = null, $email = null) {
        parent::__construct();
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->email = $email;
    }

    public function consultarTodo() {
        $sql = "SELECT * FROM usuario";
        $resultado = $this->query($sql);
        return $resultado;
    }

    public function consultarPorId($id = null) {
        if (is_numeric($id)) {
            $this->id = $id;
        }
        $dato = false;
        if (is_numeric($this->id)) {
            $sql = "SELECT * FROM usuario WHERE id = :id";
            $sentencia = $this->prepare($sql);
            $sentencia->bindValue(':id', $this->id, SQLITE3_INTEGER);
            $resultado = $sentencia->execute();
            $dato = $resultado->fetchArray();
        }
        if ($dato) {
            $this->id = $dato['id'];
            $this->nombre = $dato['nombre'];
            $this->apellido = $dato['apellido'];
            $this->email = $dato['email'];
        }
        return $dato;
    }

    public function registrar() {
        $sql = "INSERT INTO usuario (nombre, apellido, email) VALUES (:nombre, :apellido, :email)";
        $sentencia = $this->prepare($sql);
        $sentencia->bindValue(':nombre', $this->nombre, SQLITE3_TEXT);
        $sentencia->bindValue(':apellido', $this->apellido, SQLITE3_TEXT);
        $sentencia->bindValue(':email', $this->email, SQLITE3_TEXT);
        $resultado = $sentencia->execute();
        return $resultado;
    }

    public function actualizar($id = null) {
        if (is_numeric($id)) {
            $this->id = $id;
        }
        $resultado = false;
        if (is_numeric($this->id)) {
            $sql = "UPDATE usuario SET nombre = :nombre , apellido = :apellido, email = :email WHERE id = :id";
            $sentencia = $this->prepare($sql);
            $sentencia->bindValue(':nombre', $this->nombre, SQLITE3_TEXT);
            $sentencia->bindValue(':apellido', $this->apellido, SQLITE3_TEXT);
            $sentencia->bindValue(':email', $this->email, SQLITE3_TEXT);
            $sentencia->bindValue(':id', $this->id, SQLITE3_INTEGER);
            $resultado = $sentencia->execute();
        }
        return $resultado;
    }
    
    public function eliminar($id = null) {
        if (is_numeric($id)) {
            $this->id = $id;
        }
        $resultado = false;
        if (is_numeric($this->id)) {
            $sql = "DELETE FROM usuario WHERE id = :id";
            $sentencia = $this->prepare($sql);
            $sentencia->bindValue(':id', $this->id, SQLITE3_INTEGER);
            $resultado = $sentencia->execute();
        }
        return $resultado;
    }

    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getApellido() {
        return $this->apellido;
    }

    function getEmail() {
        return $this->email;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setApellido($apellido) {
        $this->apellido = $apellido;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    public function __toString() {
        return $this->nombre . ' ' . $this->apellido;
    }

}
