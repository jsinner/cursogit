<?php
namespace Modelo;
use \SQLite3;

class Modelo extends SQLite3 {

	const BASE_DE_DATOS = '/var/www/cursogit/db/curso-php.db';

	public function __construct() {
		$this->open(self::BASE_DE_DATOS);
	}

	public function crearBaseDeDatos() {
		$sql = "CREATE TABLE usuario(
                   id INTEGER PRIMARY KEY AUTOINCREMENT,
                   nombre   CHAR(50)  NOT NULL,
                   apellido CHAR(50) NOT NULL,
                   email    CHAR(100)
                );";
		$this->exec($sql);
	}

}