<?php
namespace Controlador;

/**
 * Clase padre "Controlador"
 * @author José Gabriel <jgonzalezp@me.gob.ve>
 */
abstract class Controlador {

	const RUTA_RAIZ          = '/var/www/cursogit';
	const RUTA_APP           = '/var/www/cursogit/app';
	const RUTA_VISTAS        = '/var/www/cursogit/app/vista';
	const RUTA_CONTROLADORES = '/var/www/cursogit/app/controlador';
	const RUTA_MODELOS       = '/var/www/cursogit/app/modelo';
	const RUTA_LIBS          = '/var/www/cursogit/app/libs';

	public function render($vista = 'index', $variables = array(), $showRenderView = true, $returnRenderView = true) {
		$archivoVista = self::RUTA_VISTAS.'/'.$vista.'.php';
		$renderedView = '<center><b style="color: #990000;">ERROR: Vista no encontrada!</b></center>';
		if (is_file($archivoVista)) {
			extract($variables);
			ob_start();
			require_once ($archivoVista);
			$renderedView = ob_get_clean();
		}
		if ($showRenderView) {
			echo $renderedView;
		}
		if ($returnRenderView) {
			return $renderedView;
		}
	}

	public function getPost($name, $defaultValue = null) {
		return isset($_POST[$name])?$_POST[$name]:$defaultValue;
	}

	public function getQuery($name, $defaultValue = null) {
		return isset($_GET[$name])?$_GET[$name]:$defaultValue;
	}

	public function getRequest($name, $defaultValue = null) {
		return isset($_REQUEST[$name])?$_REQUEST[$name]:$defaultValue;
	}

}