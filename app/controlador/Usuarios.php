<?php

namespace Controlador;

use Modelo\Usuario;

/**
 * Clase Controladora "Usuarios"
 * @author José Gabriel <jgonzalezp@me.gob.ve>
 */
class Usuarios extends Controlador {

    /**
     * Método por defecto del controlador Usuarios
     */
    public function lista() {
        //$db = new Modelo();
        //$db->crearBaseDeDatos();
        $usuario = new Usuario();
        $resultado = $usuario->consultarTodo();

        $this->render('lista', array(
            'mensaje' => 'Renderizando Lista de Usuarios',
            'usr' => $usuario,
            'resultado' => $resultado,
        ));
    }

    public function registro() {
        $usuario = new Usuario();
        $this->render('registro', array('usuario' => $usuario,));
    }
    
    public function actualizar() {
        $id = $this->getQuery('id');
        $usuario = new Usuario();
        $usuario->consultarPorId($id);
        $this->render('registro', array('usuario' => $usuario,));
    }
    
    public function eliminar() {
        $id = $this->getQuery('id');
        $usuario = new Usuario();
        $usuario->consultarPorId($id);
        if(strlen($usuario->getNombre())>0){
                $usuario->eliminar();
                $this->render('index', array('mensaje' => '<div class="alert alert-success">El usuario '.$usuario.' ha sido eliminado exitosamente!</div>'));                
            }else{
                $this->render('index', array('mensaje' => '<div class="alert alert-danger">Error 404 - El usuario indicado no ha sido encontrado en la base de datos</div>'));
            }
    }
    
    public function completado() {
        
        $id = $this->getPost('id');
        $nombre = $this->getPost('nombre');
        $apellido = $this->getPost('apellido');
        $email = $this->getPost('email');
        
        if(is_numeric($id) && strlen($id)>0){
            $usuario = new Usuario();
            $usuario->consultarPorId($id);
            if(strlen($usuario->getNombre())>0){
                $usuario->setNombre($nombre);
                $usuario->setApellido($apellido);
                $usuario->setEmail($email);
                $usuario->actualizar();
            }else{
                $this->render('index', array('mensaje' => '<div class="alert alert-danger">Error 404 - El usuario indicado no ha sido encontrado en la base de datos</div>'));
            }
        }
        else{
            $usuario = new Usuario($nombre, $apellido, $email);
            $usuario->registrar();
        }
        $this->render('completado', array('usuario' => $usuario));
    }

}
