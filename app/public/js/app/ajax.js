$.ajax({
    url: "https://blinding-heat-6291.firebaseio.com/usuarios.json",
    type: "GET",
    dataType: "json",
    data: null,
    beforeSend: function(){
        $("#respuesta-ajax").html('Cargando...');
        $("#respuesta-ajax").fadeIn('slow');
    },
    success: function(respuesta, estatus, dom){
        $("#respuesta-ajax").html('');
        for(var indice in respuesta){
            var usuario = respuesta[indice];
            $("#respuesta-ajax").append(
                usuario.nombre+" "+usuario.apellido+". Correo: "+usuario.correo+".</br>"
            );
        }

    },
    error: function(error, opciones, errorLanzado){
        console.log(error);
    }
});