var jose = {
    origen: 'V',
    cedula: 25555555,
    nombre: 'José Gabriel',
    apellido: 'González Pérez',
    correo: 'jgonzalezp@me.gob.ve',
    hijo: {
        nombre: 'Gabriel Junior',
        edad: 0
    },
    getFechaNacimiento: function(){
        return '1987-10-04';
    },
    casado: false
};

function Humano(origen, cedula, nombre, apellido, correo, fechaNacimiento){
    this.origen = origen;
    this.cedula = cedula;
    this.nombre = nombre;
    this.apellido = apellido;
    this.correo = correo;
    this.fechaNacimiento = fechaNacimiento;
    this.getFechaNacimiento = function(){
        return this.fechaNacimiento;
    };
}

var pedrito = new Humano('V', 123456789, 'Pedro', 'Guía', 'pedro@mail.com', '05-03-1996');

console.log(pedrito.origen);
console.log(pedrito.cedula);
console.log(pedrito.nombre);
console.log(pedrito.apellido);
console.log(pedrito.correo);
console.log(pedrito.getFechaNacimiento());
console.log(jose.hijo.nombre);
console.log(pedrito);

// $("#resultado").html("<strong>Hola</strong>");

// $(".alert").removeClass('alert-warning alert-info');
// $(".alert").addClass('alert-danger');

$(".alert").attr("class", "alert alert-danger");

$("#a-user").on('click', function(evt){
    evt.preventDefault();
    var idUser = $(this).attr("data-user-id");
    $("#div-mostrar-id").html(idUser);
});