<!DOCTYPE html>
<html>
    <head>
        <!-- aqui va el estilo esto es un comentario-->
        <meta charset="UTF-8" />
        <title>Curso de HTML y Javascript</title>
        <link href="public/img/favicon.ico" rel="shortcut icon" />
        <link rel="stylesheet" href="public/css/bootstrap.min.css" />
        <!-- Optional theme -->
        <link rel="stylesheet" href="public/css/bootstrap-theme.min.css" />
        <script src="public/js/lib/jquery.min.js"></script>
        <script src="public/js/lib/jquery-migrate.min.js"></script>
        <script src="public/js/lib/bootstrap.min.js"></script>
    </head>

    <body>

        <div class="container well col-xs-7 col-xs-offset-2">

            <div class="alert alert-info">
                Lista de Usuarios | <a class="alert-link" href="?r=Usuarios/registro">Registrar Nuevo Usuario</a>
            </div>

            <table class="bordered table">
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Email</th>
                    <th>Acciones</th>
                </tr>
<?php if ($resultado):?>
                    <?php while ($dato = $resultado->fetchArray()):?>
                <tr>
                    <td><?php echo $dato['id'];?></td>
                    <td><?php echo $dato['nombre'];?></td>
                    <td><?php echo $dato['apellido'];?></td>
                    <td><?php echo $dato['email'];?></td>
                    <td>
                        <a href="<?php echo '?r=Usuarios/actualizar&id='.$dato['id'];?>"><i class="glyphicon glyphicon-pencil success"></i></a>
                        <a href="<?php echo '?r=Usuarios/eliminar&id='.$dato['id'];?>"><i class="glyphicon glyphicon-remove danger"></i></a>
                    </td>
                </tr>
<?php endwhile;?>
                <?php  else :?>
<tr>
                    <td colspan="4"><div class="alert alert-warning">No se han conseguido registros</div></td>
                </tr>
<?php endif;?>
            </table>
        </div>

    </body>

</html>
