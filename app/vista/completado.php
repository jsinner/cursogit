<!DOCTYPE html>
<html>
    <head>
        <!-- aqui va el estilo esto es un comentario-->
        <meta charset="UTF-8" />
        <title>Curso de HTML y Javascript</title>
        <link href="public/img/favicon.ico" rel="shortcut icon" />
        <link rel="stylesheet" href="public/css/bootstrap.min.css" />
        <!-- Optional theme -->
        <link rel="stylesheet" href="public/css/bootstrap-theme.min.css" />
        <script src="public/js/lib/jquery.min.js"></script>
        <script src="public/js/lib/jquery-migrate.min.js"></script>
        <script src="public/js/lib/bootstrap.min.js"></script>
    </head>

    <body>

        <div class="container well col-xs-7 col-xs-offset-2">

            <div class="alert alert-success">
                El usuario <b><?php echo $usuario->getNombre().' '.$usuario->getApellido();
?></b> con el correo <b><?php echo $usuario->getEmail();
?></b> fue registrado exitosamente. <a href="?r=Usuarios/registro">Registrar Otro Usuario</a> ó <a href="?r=Usuarios/lista">Ver Lista de Usuarios</a>
            </div>

        </div>

    </body>

</html>