<!DOCTYPE html>
<html>
    <head>
        <!-- aqui va el estilo esto es un comentario-->
        <meta charset="UTF-8" />
        <title>Curso de HTML y Javascript</title>
        <link href="public/img/favicon.ico" rel="shortcut icon" />
        <link rel="stylesheet" href="public/css/bootstrap.min.css" />
        <!-- Optional theme -->
        <link rel="stylesheet" href="public/css/bootstrap-theme.min.css" />
        <script src="public/js/lib/jquery.min.js"></script>
        <script src="public/js/lib/jquery-migrate.min.js"></script>
        <script src="public/js/lib/bootstrap.min.js"></script>
    </head>

    <body>

        <div class="container well col-xs-7 col-xs-offset-2">
            
            <div class="alert alert-info">
                <a class="alert-link" href="?r=Usuarios/lista">Lista de Usuarios</a> | <a class="alert-link" href="?r=Usuarios/registro">Registrar Nuevo Usuario</a>
            </div>
            
            <?php if(isset($mensaje)): ?>
            <?php echo $mensaje; ?>
            <?php endif; ?>

        </div>

    </body>

</html>
