<!DOCTYPE html>
<html>
    <head>
        <!-- aqui va el estilo esto es un comentario-->
        <meta charset="UTF-8" />
        <title>Curso de HTML y Javascript</title>
        <link href="public/img/favicon.ico" rel="shortcut icon" />
        <link rel="stylesheet" href="public/css/bootstrap.min.css" />
        <!-- Optional theme -->
        <link rel="stylesheet" href="public/css/bootstrap-theme.min.css" />
        <script src="public/js/lib/jquery.min.js"></script>
        <script src="public/js/lib/jquery-migrate.min.js"></script>
        <script src="public/js/lib/bootstrap.min.js"></script>
    </head>

    <body>

        <div class="container well col-xs-7 col-xs-offset-2">

            <div class="alert alert-info">

                <i class="glyphicon glyphicon-user"></i> Formulario de Usuarios
                | <a class="alert-link" href="?r=Usuarios/lista">Vea la Lista de Usuarios</a>

            </div>

            <div id="respuesta-ajax" style="display: none;" class="alert alert-warning">
            </div>

            <form name="form-reg-usuario" action="?r=Usuarios/completado" method="POST" role="form">

                <input type="hidden" name="id" id="id" value="<?php echo $usuario->getId(); ?>" />
                
                <div class="form-group">
                    <label for="nombre">Nombre: </label>
                    <input type="text" id="nombre" name="nombre" maxlenght="50" placeholder="Introduzca su Nombre" required="required" class="form-control" value="<?php echo $usuario->getNombre(); ?>" />
                </div>
                <div class="form-group">
                    <label for="apellido">Apellido: </label>
                    <input type="text" id="apellido" name="apellido" maxlenght="50" placeholder="Introduzca su Apellido" required="required" class="form-control" value="<?php echo $usuario->getApellido(); ?>" />
                </div>
                <div class="form-group">
                    <label for="email">Email: </label>
                    <input type="email" id="email" name="email" placeholder="Introduzca su Correo Electrónico" required="required" class="form-control" value="<?php echo $usuario->getEmail(); ?>" />
                </div>
                <div class="col-xs-6">
                    <a class="btn btn-danger" href="?r=Usuarios/lista">
                        <i class="glyphicon glyphicon-arrow-left"></i> Volver
                    </a>
                </div>
                <div class="col-xs-6 text-right">
                    <button id="submit-enviar" type="submit" class="btn btn-info">
                        Guardar <i class="glyphicon glyphicon-floppy-disk"></i>
                    </button>
                </div>
            </form>
        </div>
    </body>
</html>

