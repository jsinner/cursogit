<?php
require_once ('controlador/Controlador.php');
use Controlador\Controlador;
require_once (Controlador::RUTA_LIBS.'/firebase/autoload.php');

// http://www.codeshare.io/principiosPHP

$controlador = 'Usuarios';
$accion      = 'lista';
$r           = $controlador.'/'.$accion;

if (isset($_GET['r'])) {
	$r = $_GET['r'];
}

list($controlador, $accion) = explode('/', $r);
$archivoClase               = "controlador/$controlador.php";

if (is_file($archivoClase)) {

	require_once ($archivoClase);
	$clase = "Controlador\\$controlador";
	if (class_exists($clase)) {
		$ctrl = new $clase();
		if (method_exists($ctrl, $accion)) {
			$ctrl->$accion();
		} else {
			header('Content-Type: text/html; charset=UTF-8');
			echo '<center style="color: #990000;"><h4>Error 404 - Recurso no encontrado! No se encontró la acción: <b>'.$clase.'::'.$accion.'()</b></h4></center>';
		}
	} else {
		header('Content-Type: text/html; charset=UTF-8');
		echo '<center style="color: #990000;"><h4>Error 404 - Recurso no encontrado! No se encontró el controlador: <b>'.$clase.'</b></h4></center>';
	}
} else {
	header('Content-Type: text/html; charset=UTF-8');
	echo '<center style="color: #990000;"><h4>Error 404 - Recurso no encontrado! </h4></center>';
}

?>