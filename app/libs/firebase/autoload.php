<?php
use Controlador\Controlador;

class AutoLoader {

	private $mapper;

	public function __construct() {
		$this->mapper = array('Firebase' => Controlador::RUTA_LIBS.'/firebase/src', 'Controlador' => Controlador::RUTA_CONTROLADORES, 'Modelo' => Controlador::RUTA_MODELOS);
		spl_autoload_register(array($this, 'classLoader'));
	}

	public function classLoader($class) {
		if (class_exists($class, false)) {
			return true;
		}

		// break into single namespace and class name
		$classparts = explode('\\', $class);

		// the package namespace
		$ns         = $classparts[0];
		$base_dir   = $this->mapper[$ns];
		$prefix_len = strlen($ns);
		if (substr($class, 0, $prefix_len) !== $ns) {
			return;
		}
		// strip the prefix off the class
		$class = substr($class, $prefix_len);
		// a partial filename
		$file = $base_dir.str_replace('\\', DIRECTORY_SEPARATOR, $class).'.php';
		if (is_readable($file)) {
			require $file;
		}
	}
}

$autoloader = new AutoLoader();